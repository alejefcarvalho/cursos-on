from xml.etree.ElementInclude import include
from django.contrib import admin
from rest_framework import routers
from django.urls import path, include
from app.api import viewsets

route = routers.DefaultRouter()
route.register(r'courses', viewsets.CoursesViewSet)
route.register(r'students', viewsets.StudentsViewSet)

# from app.views import createCourse, deleteCourse, editCourse, formCourse, home, index, form, create, indexCourse, updateCourse, view, edit, update, delete, viewCourse

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(route.urls)),

    # path('', home, name= 'home'),
    # path('index', index, name= 'index'),
    # path('form/', form, name= 'form'), 
    # path('create/', create, name= 'create'),
    # path('view/<int:pk>/', view, name= 'view'),
    # path('edit/<int:pk>/', edit, name= 'edit'),
    # path('update/<int:pk>/', update, name= 'update'),
    # path('delete/<int:pk>/', delete, name= 'delete'),

    # path('indexCourses', indexCourse, name= 'indexCourses'),
    # path('formCourses/', formCourse, name= 'formCourses'), 
    # path('createCourses/', createCourse, name= 'createCourses'),
    # path('viewCourses/<int:pk>/', viewCourse, name= 'viewCourses'),
    # path('editCourses/<int:pk>/', editCourse, name= 'editCourses'),
    # path('updateCourses/<int:pk>/', updateCourse, name= 'updateCourses'),
    # path('deleteCourses/<int:pk>/', deleteCourse, name= 'deleteCourses'),
]
