from django.db.models.deletion import CASCADE
from django.db import models


class Course(models.Model):
    name= models.CharField(max_length=255)
    description= models.CharField(max_length=255)
    
    def __str__(self):
        return self.name

class Student(models.Model):
    first_name = models.CharField(max_length=255)
    last_name= models.CharField(max_length=255)
    age= models.IntegerField()
    courses = models.ForeignKey(Course,on_delete=CASCADE, related_name='course_student')
    
    def __str__(self):
        return self.first_name

