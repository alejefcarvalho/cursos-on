from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from app.api.serializers import CoursesSerializer, StudentsSerializer
from app.models import Student, Course


class StudentsViewSet(viewsets.ModelViewSet):
    serializer_class = StudentsSerializer
    queryset = Student.objects.all()

    @api_view(['GET', 'POST'])
    def student_list(request):
        """
        List  Students, or create a new students.
        """
        if request.method == 'GET':
            data = []
            nextPage = 1
            previousPage = 1
            students = Student.objects.all()
            page = request.GET.get('page', 1)
            paginator = Paginator(students, 10)
            try:
                data = paginator.page(page)
            except PageNotAnInteger:
                data = paginator.page(1)
            except EmptyPage:
                data = paginator.page(paginator.num_pages)

            serializer = StudentsSerializer(data,context={'request': request} ,many=True)
            if data.has_next():
                nextPage = data.next_page_number()
            if data.has_previous():
                previousPage = data.previous_page_number()
            
            return Response({'data': serializer.data , 'count': paginator.count, 'numpages' : paginator.num_pages, 'nextlink': '/api/students/?page=' + str(nextPage), 'prevlink': '/api/students/?page=' + str(previousPage)})

        elif request.method == 'POST':
            serializer = StudentsSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    ...
    @api_view(['GET', 'PUT', 'DELETE'])
    def students_detail(request, id):
        """
        Retrieve, update or delete a student by id/id.
        """
        try:
            student = Student.objects.get(id=id)
        except Student.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if request.method == 'GET':
            serializer = StudentsSerializer(student,context={'request': request})
            return Response(serializer.data)

        elif request.method == 'PUT':
            serializer = StudentsSerializer(student, data=request.data,context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'DELETE':
            student.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)


""" Courses """

class CoursesViewSet(viewsets.ModelViewSet):
    serializer_class = CoursesSerializer
    queryset = Course.objects.all()

    @api_view(['GET', 'POST'])
    def courses_list(request):
        """
        List  Course, or create a new course.
        """
        if request.method == 'GET':
            data = []
            nextPage = 1
            previousPage = 1
            courses = Course.objects.all()
            page = request.GET.get('page', 1)
            paginator = Paginator(courses, 10)
            try:
                data = paginator.page(page)
            except PageNotAnInteger:
                data = paginator.page(1)
            except EmptyPage:
                data = paginator.page(paginator.num_pages)

            serializer = CoursesSerializer(data,context={'request': request} ,many=True)
            if data.has_next():
                nextPage = data.next_page_number()
            if data.has_previous():
                previousPage = data.previous_page_number()
            
            return Response({'data': serializer.data , 'count': paginator.count, 'numpages' : paginator.num_pages, 'nextlink': '/api/courses/?page=' + str(nextPage), 'prevlink': '/api/courses/?page=' + str(previousPage)})

        elif request.method == 'POST':
            serializer = CoursesSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    ...
    @api_view(['GET', 'PUT', 'DELETE'])
    def course_detail(request, id):
        """
        Retrieve, update or delete a course by id/id.
        """
        try:
            course = Course.objects.get(id=id)
        except Course.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if request.method == 'GET':
            serializer = CoursesSerializer(course,context={'request': request})
            return Response(serializer.data)

        elif request.method == 'PUT':
            serializer = CoursesSerializer(course, data=request.data,context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'DELETE':
            course.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)