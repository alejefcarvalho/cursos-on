from rest_framework import serializers
from app.models import Course, Student


class CoursesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id','name', 'description')

class StudentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id','first_name', 'last_name', 'age', 'courses')







