from django.forms import ModelForm
from app.models import Course, Student


class StudentForm(ModelForm):
     class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'age', 'courses']

class CourseForm(ModelForm):
     class Meta:
        model = Course
        fields = ['name', 'description']