from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from app.forms import CourseForm, StudentForm
from app.models import Student, Course

def home(request):
    return render(request, 'home.html')

def index(request):
    data={}
    search = request.GET.get('search')
    if search:
        data['db'] = Student.objects.filter(first_name__icontains=search) or Student.objects.filter(last_name__icontains=search) \
                     or Student.objects.filter(age__icontains=search) or Student.objects.filter(courses__icontains=search)
    else:
        data['db'] = Student.objects.all()
        all =  Student.objects.all()
        paginator = Paginator(all, 5)
        pages = request.GET.get('page')
        data['db']= paginator.get_page(pages)
    data['index'] = StudentForm
    return render(request, 'index.html', data)

def form(request):
    data = {}
    data['form'] = StudentForm
    return render(request, 'form.html', data)

def create(request):
    form = StudentForm(request.POST or None)
    if form.is_valid():
        form.save() 
        return redirect('index')

def view(request, pk):
    data = {}
    data['db']= Student.objects.get(pk=pk)
    return render(request, 'view.html', data) 

def edit(request, pk):
    data = {}
    data['db']= Student.objects.get(pk=pk)
    data['form'] = StudentForm(instance= data['db'])
    return render(request, 'form.html',data)

def update(request, pk):
    data={}
    data['db'] = Student.objects.get(pk=pk)
    form = StudentForm(request.POST or None, instance=data['db'])
    if form.is_valid():
        form.save()
    return redirect('index')


def delete(request, pk):
        db = Student.objects.get(pk=pk)
        db.delete()
        return redirect('index')

# Courses

def indexCourse(request):
    data={}
    search = request.GET.get('search')
    if search:
        data['db'] = Course.objects.filter(name__icontains=search) or Course.objects.filter(description__icontains=search)
    else:
        data['db'] = Course.objects.all()
        all =  Course.objects.all()
        paginator = Paginator(all, 5)
        pages = request.GET.get('page')
        data['db']= paginator.get_page(pages)
    data['indexCourses'] = CourseForm
    return render(request, 'indexCourses.html', data)

def formCourse(request):
    data = {}
    data['formCourses'] = CourseForm
    return render(request, 'formCourses.html', data)

def createCourse(request):
    form = CourseForm(request.POST or None)
    if form.is_valid():
        form.save() 
        return redirect('indexCourses')

def viewCourse(request, pk):
    data = {}
    data['db']= Course.objects.get(pk=pk)
    return render(request, 'viewCourses.html', data) 

def editCourse(request, pk):
    data = {}
    data['db']= Course.objects.get(pk=pk)
    data['form'] = CourseForm(instance= data['db'])
    return render(request, 'formCourses.html',data)

def updateCourse(request, pk):
    data={}
    data['db'] = Course.objects.get(pk=pk)
    form = CourseForm(request.POST or None, instance=data['db'])
    if form.is_valid():
        form.save()
    return redirect('indexCourses')


def deleteCourse(request, pk):
        db = Course.objects.get(pk=pk)
        db.delete()
        return redirect('indexCourses')