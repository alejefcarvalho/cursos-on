# Teste para processo seletivo 

## Instalações necessárias
<!--ts-->
   * [Instalação](#instalacao)
        * [Poetry](#Poetry)
<!--te-->

## Para acelerar o desenvolvimento as tebelas foram criadas no SQLite
        Depois de realizar as instalações necessarias, basta rodar os seguintes comandos.
<!--ts-->        
# poetry install

# python manage.py migrate

# python manage.py runserver
<!--te-->

(Acessar o link)  [http://localhost:8000/]

## Caso queira testar o projeto criado utilizando os templates do Django,
## basta descomentar as linhas 11 e da 17 a 32  no arquivo urls.py dentro da pasta student_project.

## E comentar as linhas 7, 8, 9 e 15



